//
//  UserApi.swift
//  MChat
//
//  Created by Михаил on 30.11.2019.
//  Copyright © 2019 Mikhail Kolotilin. All rights reserved.
//

import Foundation
import FirebaseAuth
import Firebase
import FirebaseStorage
import ProgressHUD

class UserApi {
    
    var currentUserId: String {
        return Auth.auth().currentUser != nil ? Auth.auth().currentUser!.uid : ""
    }
    
    func signIn(email: String, password: String, onSucces: @escaping() -> Void, onError: @escaping(_ errorMesage: String) -> Void) {
        Auth.auth().signIn(withEmail: email, password: password) { (authData, error) in
            if error != nil {
                onError(error!.localizedDescription)
                return
            }
            onSucces()
        }
    }
    
    func singUp(withUserName username: String, email: String, password: String, image:UIImage?, onSucces: @escaping() -> Void, onError: @escaping(_ errorMesage: String) -> Void) {
        Auth.auth().createUser(withEmail: email, password: password) { (authDataResult, error) in
            if error != nil {
                ProgressHUD.showError(error!.localizedDescription)
                return
            }
            if let authData = authDataResult {
                let dict: Dictionary<String, Any> = [
                    UID: authData.user.uid,
                    EMAIL: authData.user.email,
                    USERNAME: username,
                    PROFILE_IMAGE_URL: "",
                    STATUS: "Welcome to MChat"
                ]
                
                guard let imageSelected = image else {
                    ProgressHUD.showError(ERROR_EMPTY_PHOTO)
                    return
                }
                // Конвертируем и сжимаем imageSelected для добавления в БД
                guard let imageData = imageSelected.jpegData(compressionQuality: 0.4) else { return }
                
                let storageProfile = Ref().storageSpecificProfile(uid: authData.user.uid)
                
                let metaData = StorageMetadata()
                
                metaData.contentType = "image/jpg"
                
                StorageService.savePhoto(username: username, uid: authData.user.uid, data: imageData, metadeta: metaData, storageProfileRef: storageProfile, dict: dict, onSucces: {
                    onSucces()
                }) { (errorMessage) in
                    onError(errorMessage)
                }
                
            }
        }
    }
    
    func resetPassword(email: String, onSucces: @escaping() -> Void, onError: @escaping(_ errorMesage: String) -> Void) {
        Auth.auth().sendPasswordReset(withEmail: email) { (error) in
            if error == nil {
                onSucces()
            } else {
                onError(error!.localizedDescription)
            }
        }
    }
    
    func logOut() {
        do {
            try Auth.auth().signOut()
        } catch {
            ProgressHUD.showError(error.localizedDescription)
            return
        }
        (UIApplication.shared.delegate as! AppDelegate).configureInitialViewController()
    }
    
    func observeUsers(onSucces: @escaping(UserCompletion)) {
        Ref().databaseUsers.observe(.childAdded) { (snapshot) in
            if let dict = snapshot.value as? Dictionary<String,Any> {
                if let user = User.transformUser(dict: dict) {
                    onSucces(user)
                }
            }
        }
    }
}

typealias UserCompletion = (User) -> Void
