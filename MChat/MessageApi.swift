//
//  MessageApi.swift
//  MChat
//
//  Created by Михаил on 02.12.2019.
//  Copyright © 2019 Mikhail Kolotilin. All rights reserved.
//

import Foundation
import Firebase

class MessageApi {
    func sendMessage(from: String, to: String, value: Dictionary<String,Any>) {
        let ref = Ref().databaseMessageSendTo(from: from, to: to)
        ref.childByAutoId().updateChildValues(value)
    }
}
