//
//  StorageService.swift
//  MChat
//
//  Created by Михаил on 30.11.2019.
//  Copyright © 2019 Mikhail Kolotilin. All rights reserved.
//

import Foundation
import FirebaseStorage
import FirebaseDatabase
import FirebaseAuth
import ProgressHUD

class StorageService {
    static func  savePhoto(username: String, uid: String, data: Data, metadeta: StorageMetadata, storageProfileRef: StorageReference, dict: Dictionary<String,Any>, onSucces: @escaping() -> Void, onError: @escaping(_ errorMesage: String) -> Void) {
        
        // Загрузка картинок в БД
        storageProfileRef.putData(data, metadata: metadeta, completion: { (storageMetaData, error) in
            if error != nil {
                onError(error!.localizedDescription)
                return
            }
            
            storageProfileRef.downloadURL(completion: { (url, error) in
                if let metaImageUrl = url?.absoluteString {
                    
                    if let changeRequest = Auth.auth().currentUser?.createProfileChangeRequest() {
                        changeRequest.photoURL = url
                        changeRequest.displayName = username
                        changeRequest.commitChanges { (error) in
                            if let error = error {
                                ProgressHUD.showError(error.localizedDescription)
                            }
                        }
                    }
                    
                    var dictTemp = dict
                    dictTemp[PROFILE_IMAGE_URL] = metaImageUrl
                    
                    Ref().databaseSpecificUser(uid: uid).updateChildValues(dictTemp, withCompletionBlock: { (error, ref) in
                            if error == nil {
                                onSucces()
                            } else {
                                onError(error!.localizedDescription)
                            }
                        })
                }
            })
        })
    }
}
