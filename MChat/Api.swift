//
//  Api.swift
//  MChat
//
//  Created by Михаил on 30.11.2019.
//  Copyright © 2019 Mikhail Kolotilin. All rights reserved.
//

import Foundation

struct Api {
    static var User = UserApi()
    static var Message = MessageApi()
}
