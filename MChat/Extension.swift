//
//  Extension.swift
//  MChat
//
//  Created by Михаил on 01.12.2019.
//  Copyright © 2019 Mikhail Kolotilin. All rights reserved.
//

import Foundation
import SDWebImage

extension UIImageView {
    func loadImage(_ urlString: String?, onSucces: ((UIImage) -> Void)? = nil) {
        self.image = UIImage()
        guard let string = urlString else { return }
        guard let url = URL(string: string) else { return }
        
        self.sd_setImage(with: url) { (image, error, type, url) in
            if onSucces != nil, error == nil {
                onSucces!(image!)
            }
        }
    }
}
